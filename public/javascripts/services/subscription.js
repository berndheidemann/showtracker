angular.module('MyApp')
  .factory('Subscription', ['$http', function($http) {
    return {
      subscribe: function(show, user) {
        return $http.post('/api/subscription/subscribe', { showId: show._id });
      },
      unsubscribe: function(show, user) {
        return $http.post('/api/subscription/unsubscribe', { showId: show._id });
      }
    };
  }]);