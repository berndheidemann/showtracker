angular.module('MyApp')
	.controller('ProfileCtrl', ['$scope', '$rootScope', '$routeParams', '$location', 'User',
		function($scope, $rootScope, $routeParams, $location, User) {

			if (!$rootScope.currentUser) { 
				$location.path('/login');
			} else {
				User.get({
					_id: $rootScope.currentUser._id
				}, function(response) {
					$scope.user = response.user;
					$scope.shows = response.shows;
				});
			}

			$scope.editProfile = function() {
				$scope.edit = true;
			};

			$scope.saveProfile = function() {
				//todo
				$scope.edit = false;
			};
		}
	]);