var express = require('express');
var router = express.Router();
var User = require('../models/userModel.js');
var Show = require('../models/showModel.js');
var ObjectId=require('mongoose').Schema.ObjectId;
var util = require("util");

router.get("/", function(req, res, next) {
	console.log("user route geht");
});

router.get('/:id', function(req, res, next) {
  User.findById(req.params.id)
  	.exec()
  	.then( function(user) {
  		console.log(" fetching shows for user " + user._id);

  		Show.find({subscribers: user._id})
  			.exec()
  			.then(function(shows) {
  				
  				user.shows = shows;
  				user.test = "asd";
  				
  				
  				console.log(util.inspect(user.shows[0].name));
  				res.send({user: user, shows: shows});
  			})
  			.catch(function(error) {
  				console.log("ERROR fetching shows for user " + user._id);
  				next(error);
  			});
 	 })
  	.catch(function(error) {
  		next(error);
  	});
});


module.exports = router;



